//
//  ForthViewController.swift
//  nixi
//
//  Created by munetomoissei on 2017/09/03.
//  Copyright © 2017年 munetomoissei. All rights reserved.
//

import UIKit
import AVFoundation

class ForthViewController: UIViewController,AVCapturePhotoCaptureDelegate {

    var imageView: UIImageView!
    // プレビュー用のビューとOutlet接続しておく
    @IBOutlet weak var previewView: UIView!
    // インスタンスの作成
    var session = AVCaptureSession()
    var photoOutput = AVCapturePhotoOutput()
    // 通知センターを作る
    let notification = NotificationCenter.default
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // UIImageViewのサイズを設定する
        let iWidth: CGFloat = 300
        let iHeight: CGFloat = 100
        
        // UIImageViewのx,yを設定する
        let posX: CGFloat = (self.view.bounds.width - iWidth)/2
        let posY: CGFloat = (self.view.bounds.height - iHeight)/2
        
        // UIImageViewを作成する.
        imageView = UIImageView(frame: CGRect(x: posX, y: posY, width: iWidth, height: iHeight))
        
        self.makeShutterButton()
        
        // UIImageViewをViewに追加する.
        self.view.addSubview(imageView)
        
        // セッション実行中ならば中断する
        if session.isRunning {
            return
        }
        // 入出力の設定
        setupInputOutput()
        // プレビューレイヤの設定
        setPreviewLayer()
        // セッション開始
        session.startRunning()
        // デバイスが回転したときに通知するイベントハンドラを設定する
        notification.addObserver(self,
                                 selector: #selector(self.changedDeviceOrientation(_:)),
                                 name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)

    }

    // 画像アップロードボタンを設置する
    func makeShutterButton (){
        
        // Buttonを生成する.
        let uploadButton = UIButton()
        uploadButton.frame = CGRect(x:0,y:50,width:200,height:40)
        uploadButton.backgroundColor = UIColor.red
        uploadButton.layer.masksToBounds = true
        uploadButton.setTitle("シャッター", for: UIControlState.normal)
        uploadButton.setTitleColor(UIColor.white, for: UIControlState.normal)
        uploadButton.setTitle("シャッター", for: UIControlState.highlighted)
        uploadButton.setTitleColor(UIColor.black, for: UIControlState.highlighted)
        uploadButton.layer.cornerRadius = 20.0
        uploadButton.layer.position = CGPoint(x: self.view.frame.width/2, y:200)
        uploadButton.tag = 1
        uploadButton.addTarget(self, action: #selector(self.homeShutter), for:.touchDown)
        self.view.addSubview(uploadButton)
    }
    
    // ファイルをアップロードする
    func homeShutter (){
        let captureSetting = AVCapturePhotoSettings()
        captureSetting.flashMode = .auto
        captureSetting.isAutoStillImageStabilizationEnabled = true
        captureSetting.isHighResolutionPhotoEnabled = false
        // キャプチャのイメージ処理はデリゲートに任せる
        photoOutput.capturePhoto(with: captureSetting, delegate: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // 入出力の設定
    func setupInputOutput(){
        //解像度の指定
        session.sessionPreset = AVCaptureSessionPresetPhoto
        
        // 入力の設定
        do {
            //デバイスの取得
            let device = AVCaptureDevice.defaultDevice(
                withDeviceType: AVCaptureDeviceType.builtInWideAngleCamera,
                mediaType: AVMediaTypeVideo,
                position: .back)
            
            // 入力元
            let input = try AVCaptureDeviceInput(device: device)
            if session.canAddInput(input){
                session.addInput(input)
            } else {
                print("セッションに入力を追加できなかった")
                return
            }
        } catch  let error as NSError {
            print("カメラがない \(error)")
            return
        }
        
        // 出力の設定
        if session.canAddOutput(photoOutput) {
            session.addOutput(photoOutput)
        } else {
            print("セッションに出力を追加できなかった")
            return
        }
    }
    
    // プレビューレイヤの設定
    func setPreviewLayer(){
        // プレビューレイヤを作る
        let previewLayer = AVCaptureVideoPreviewLayer(session: session)
        guard let videoLayer = previewLayer else {
            print("プレビューレイヤを作れなかった")
            return
        }
        videoLayer.frame = view.bounds
        videoLayer.masksToBounds = true
        videoLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        // previewViewに追加する
        previewView.layer.addSublayer(videoLayer)
    }
    
    // デバイスの向きが変わったときに呼び出すメソッド
    func changedDeviceOrientation(_ notification :Notification) {
        // photoOutput.connectionの回転向きをデバイスと合わせる
        if let photoOutputConnection = self.photoOutput.connection(withMediaType: AVMediaTypeVideo) {
            switch UIDevice.current.orientation {
            case .portrait:
                photoOutputConnection.videoOrientation = .portrait
            case .portraitUpsideDown:
                photoOutputConnection.videoOrientation = .portraitUpsideDown
            case .landscapeLeft:
                photoOutputConnection.videoOrientation = .landscapeRight
            case .landscapeRight:
                photoOutputConnection.videoOrientation = .landscapeLeft
            default:
                break
            }
        }
    }
    
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
