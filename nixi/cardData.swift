//
//  cardData
//  nixi
//
//  Created by munetomoissei on 2017/01/19.
//  Copyright © 2017年 munetomoissei. All rights reserved.
//

import Foundation
import RealmSwift

// カードに表示するデータを保存するDTOクラス
class cardData: Object {
    dynamic var id = "";
    dynamic var name = "";
    dynamic var image:Data = Data()
    dynamic var country = ""
    dynamic var city = ""
    dynamic var age = ""
    // idをプライマリキーに設定
    override static func primaryKey() -> String? {
        return "id"
    }
}
