//
//  SecondViewController.swift
//  nixi
//
//  Created by munetomoissei on 2017/07/17.
//  Copyright © 2017年 munetomoissei. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage
import FirebaseDatabase

class SecondViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // UIImageViewのサイズを設定する
        let iWidth: CGFloat = 300
        let iHeight: CGFloat = 100
        
        // UIImageViewのx,yを設定する
        let posX: CGFloat = (self.view.bounds.width - iWidth)/2
        let posY: CGFloat = (self.view.bounds.height - iHeight)/2
        
        // UIImageViewを作成する.
        imageView = UIImageView(frame: CGRect(x: posX, y: posY, width: iWidth, height: iHeight))
        
        self.makeUploadButton()
       
        // UIImageViewをViewに追加する.
        self.view.addSubview(imageView)
    }
    
    // 画像アップロードボタンを設置する
    func makeUploadButton (){
        // Buttonを生成する.
        let openButton = UIButton()
        openButton.frame = CGRect(x:0,y:0,width:200,height:40)
        openButton.backgroundColor = UIColor.red
        openButton.layer.masksToBounds = true
        openButton.setTitle("画像を選択する", for: UIControlState.normal)
        openButton.setTitleColor(UIColor.white, for: UIControlState.normal)
        openButton.setTitle("画像を選択する", for: UIControlState.highlighted)
        openButton.setTitleColor(UIColor.black, for: UIControlState.highlighted)
        openButton.layer.cornerRadius = 20.0
        openButton.layer.position = CGPoint(x: self.view.frame.width/2, y:100)
        openButton.tag = 1
        openButton.addTarget(self, action: #selector(self.homeButttonAlbumOpen), for:.touchDown)
        self.view.addSubview(openButton)
        
        // Buttonを生成する.
        let uploadButton = UIButton()
        uploadButton.frame = CGRect(x:0,y:50,width:200,height:40)
        uploadButton.backgroundColor = UIColor.red
        uploadButton.layer.masksToBounds = true
        uploadButton.setTitle("アップロードする", for: UIControlState.normal)
        uploadButton.setTitleColor(UIColor.white, for: UIControlState.normal)
        uploadButton.setTitle("アップロードする", for: UIControlState.highlighted)
        uploadButton.setTitleColor(UIColor.black, for: UIControlState.highlighted)
        uploadButton.layer.cornerRadius = 20.0
        uploadButton.layer.position = CGPoint(x: self.view.frame.width/2, y:200)
        uploadButton.tag = 1
        uploadButton.addTarget(self, action: #selector(self.homeImageUpload), for:.touchDown)
        self.view.addSubview(uploadButton)
    }
    
    // ファイルをアップロードする
    func homeImageUpload (){
        let storage = Storage.storage()
        let storageRef = storage.reference(forURL: "gs://nixiapp-8c84d.appspot.com")
        
        // ツリーの下位への参照を作成
        let imageRef = storageRef.child("image.jpg")
        
        // Dataを作成
        let imageData = UIImageJPEGRepresentation(self.imageView.image!, 1.0)
        
        if let data = imageData {
            // ここでアップするパス、ファイル名を決める
            let riversRef = storageRef.child("user/test.jpg")
            let uploadTask = riversRef.putData(data, metadata: nil) { (metadata, error) in
                guard let metadata = metadata else {
                    // Uh-oh, an error occurred!
                    return
                }
            }
        }
    }
    
    // アルバムから画像をアップする
    func homeButttonAlbumOpen (sender: UIButton){
        // アルバム起動！
        let album = UIImagePickerControllerSourceType.photoLibrary
        if UIImagePickerController.isSourceTypeAvailable(album) {
            let album = UIImagePickerController()
            album.delegate = self
            album.sourceType = UIImagePickerControllerSourceType.photoLibrary
            album.allowsEditing = true
            self.present(album, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        self.imageView.image = image
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        self.dismiss(animated: true, completion: nil)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

