//
//  FirstViewController.swift
//  nixi
//
//  Created by munetomoissei on 2017/07/17.
//  Copyright © 2017年 munetomoissei. All rights reserved.
//

import UIKit
import Koloda
import Cartography
import Firebase
import FirebaseStorage
import FirebaseDatabase
import Realm
import RealmSwift

class FirstViewController: UIViewController,KolodaViewDelegate {
    
    let kolodaView = KolodaView()
    let headerView = UIView()
    var viewW = 0.0
    
    // いいね数、nixipointを表示する用のイメージビュー
    var goodImage = UIImage()
    var goodImageView = UIImageView()
    var nixiPointImage = UIImage()
    var nixiPointImageView = UIImageView()
    
    // いいね5パターンのイメージビュー
    var reloadImage = UIImage()
    var reloadImageView = UIImageView()
    
    var armImage = UIImage()
    var armImageView = UIImageView()
    
    var proteinImage = UIImage()
    var proteinImageView = UIImageView()
    
    var bodyImage = UIImage()
    var bodyImageView = UIImageView()
    
    var barbelImage = UIImage()
    var barbelImageView = UIImageView()
    
    // firebaseの変数の設定
    let ref = Database.database().reference()

    // relamインスタンスの初期化
    let realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewW = Double(self.view.frame.width)
        
        let cardX = self.viewW*0.031
        let cardY = self.viewW*0.168
        let cardW = self.viewW*0.937
        let cardH = self.viewW*1.237
        
        
        // カードデータの初期化
        self.realm.deleteAll()



        // カードのビューの定義
        self.kolodaView.frame = CGRect(x:cardX, y:cardY, width:cardW, height:cardH)
        kolodaView.dataSource = self as! KolodaViewDataSource
        kolodaView.delegate = self as! KolodaViewDelegate
        self.modalTransitionStyle = UIModalTransitionStyle.flipHorizontal
        self.view.addSubview(self.kolodaView)

        // 各種ビューの設定
        self.setViewContent()

        // 各種いいねアイコンを設定
        self.setGoodIcon()
        
        self.ref.observe(.childAdded, with: { (snapshot) in
                print(snapshot)
            })
    }
    
    // カード1つ1つの設定
    override func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
        let view = UIView(frame: koloda.bounds)
        
        // 背景色
        view.backgroundColor = UIColor.white
        view.layer.borderWidth = 1.0
        view.layer.borderColor = UIColor(red:230.0/255,green:230.0/255,blue:230.0/255,alpha:1.0).cgColor
        view.layer.cornerRadius = 7
        
        // 投稿画像
        let cardImage:UIImage? = UIImage(named: "ronald.jpg")
        let cardImageView       = UIImageView(image:cardImage)
        cardImageView.layer.position = CGPoint(x:0, y:0)
        cardImageView.frame.size = CGSize(width: 0, height: 0)
        view.addSubview(cardImageView)
        
        // 制約を設定
        constrain(cardImageView) { view in
            view.top   == view.superview!.top + 10
            view.width == view.superview!.width*0.82
            view.centerX == view.superview!.centerX
            view.height == view.superview!.height*0.82
        }
        
        // 名前のラベルを設定
        let nameLabel = UILabel()
        nameLabel.frame = CGRect(x:0, y:0, width:0, height:0)
        nameLabel.text = "ronald"
        nameLabel.font = UIFont.systemFont(ofSize: 20.0)
        nameLabel.sizeToFit()
        view.addSubview(nameLabel)
        
        // 年齢のラベルを設定
        let ageLabel = UILabel()
        ageLabel.frame = CGRect(x:0, y:0, width:0, height:0)
        ageLabel.text = "32"
        ageLabel.font = UIFont.systemFont(ofSize: 20.0)
        view.addSubview(ageLabel)
        
        // 都市のラベルを設定
        let cityLabel = UILabel()
        cityLabel.frame = CGRect(x:0, y:0, width:0, height:0)
        cityLabel.text = "yokohama"
        cityLabel.font = UIFont.systemFont(ofSize: 15.0)
        cityLabel.textColor = UIColor(red:171.0/255,green:171.0/255,blue:171.0/255,alpha:1.0)
        view.addSubview(cityLabel)
        
        // 国のラベルを設定
        let countryLabel = UILabel()
        countryLabel.frame = CGRect(x:0, y:0, width:0, height:0)
        countryLabel.text = "Japan"
        countryLabel.font = UIFont.systemFont(ofSize: 15.0)
        countryLabel.textColor = UIColor(red:171.0/255,green:171.0/255,blue:171.0/255,alpha:1.0)
        view.addSubview(countryLabel)
        
        // いいね数のラベルを設定
        let goodValueLabel = UILabel()
        goodValueLabel.frame = CGRect(x:0, y:0, width:0, height:0)
        goodValueLabel.text = "32"
        goodValueLabel.font = UIFont.systemFont(ofSize: 20.0)
        view.addSubview(goodValueLabel)
        
        // nixi値のラベルを設定
        let nixiPointLabel = UILabel()
        nixiPointLabel.frame = CGRect(x:0, y:0, width:0, height:0)
        nixiPointLabel.text = "32"
        nixiPointLabel.font = UIFont.systemFont(ofSize: 20.0)
        view.addSubview(nixiPointLabel)
        
        // いいね数、nixipoint数のアイコン
        goodImage = UIImage(named: "like")!
        goodImageView       = UIImageView(image:goodImage)
        goodImageView.layer.position = CGPoint(x:0, y:0)
        goodImageView.frame.size = CGSize(width: 0, height: 0)
        
        nixiPointImage = UIImage(named: "nixipoint")!
        nixiPointImageView       = UIImageView(image:nixiPointImage)
        nixiPointImageView.layer.position = CGPoint(x:0, y:0)
        nixiPointImageView.frame.size = CGSize(width: 0, height: 0)
        
        // 各ラベルの制約を設定
        constrain(nameLabel,ageLabel,cityLabel,countryLabel,cardImageView) { view1,view2,view3,view4,cardView in
            
            view1.left   == cardView.left
            view2.left   == view1.right + 3
            
            view1.top   == cardView.bottom + 9
            view2.top   == view1.top
            
            view3.top   == view1.bottom + 7
            view3.left   == view1.left
            
            view4.top   == view3.top
            view4.left   == view3.right + 3
        }
        
        // いいね数、nixipoint数のアイコンの設定
        view.addSubview(goodImageView)
        view.addSubview(self.nixiPointImageView)
        
        // 各ラベルの制約を設定
        constrain(nameLabel,self.goodImageView,self.nixiPointImageView) { view1,view2,view3 in
            view2.top   == view1.top
            view3.top   == view1.top
        }
        
        // 各ラベルの制約を設定
        constrain(self.goodImageView) { view in
            view.left  == view.superview!.centerX + 30
        }
        
        // 各ラベルの制約を設定
        constrain(self.nixiPointImageView,self.goodImageView) { view1,view2 in
            view1.top  == view2.top
            view1.left  == view2.left + 60
        }
        
        // 各ラベルの制約を設定
        constrain(self.nixiPointImageView,self.goodImageView) { view1,view2 in
            view1.top  == view2.top 
            view1.left  == view2.left + 60
        }
        
        // 各ラベルの制約を設定
        constrain(self.nixiPointImageView,self.goodImageView,nixiPointLabel,goodValueLabel) { view1,view2,view3,view4 in
            view3.left == view1.right + 5
            view3.top == view1.top
            view4.left == view2.right + 5
            view4.top == view1.top
        }
        
        return view
    }
    
    // 各種ビューの設定
    func setViewContent(){
        
        // 背景色
        self.view.backgroundColor = UIColor(red:240.0/255,green:240.0/255,blue:240.0/255,alpha:1.0)
        
        // ヘッダーの設定
        headerView.frame = CGRect(x:0, y:0, width:0, height:0)
        headerView.backgroundColor = UIColor(red:208.0/255,green:1.0/255,blue:27.0/255,alpha:1.0)
        self.view.addSubview(headerView)
        // 制約を設定
        constrain(headerView) { view in
            view.height == view.superview!.height*0.08
            view.width == view.superview!.width
        }
        
        // ロゴ画像
        let logoImage:UIImage? = UIImage(named: "logo")
        let logoImageView       = UIImageView(image:logoImage)
        logoImageView.layer.position = CGPoint(x:0, y:0)
        logoImageView.frame.size = CGSize(width: 0, height: 0)
        headerView.addSubview(logoImageView)
        
        // 制約を設定
        constrain(logoImageView) { view in
            view.center == view.superview!.center
        }
        
        
    }
    
    func setGoodIcon(){
        
        var ovalImage = UIImage()
        ovalImage = UIImage(named: "oval")!
        
        var ovalImageView1 = UIImageView()
        var ovalImageView2 = UIImageView()
        var ovalImageView3 = UIImageView()
        var ovalImageView4 = UIImageView()
        var ovalImageView5 = UIImageView()
        
        ovalImageView1       = UIImageView(image:ovalImage)
        ovalImageView1.layer.position = CGPoint(x:0, y:0)
        ovalImageView1.frame.size = CGSize(width: self.viewW*0.125, height: self.viewW*0.125)
        self.view.addSubview(ovalImageView1)
        
        ovalImageView2       = UIImageView(image:ovalImage)
        ovalImageView2.layer.position = CGPoint(x:0, y:0)
        ovalImageView2.frame.size = CGSize(width: 0, height: 0)
        self.view.addSubview(ovalImageView2)
        
        ovalImageView3       = UIImageView(image:ovalImage)
        ovalImageView3.layer.position = CGPoint(x:0, y:0)
        ovalImageView3.frame.size = CGSize(width: 0, height: 0)
        self.view.addSubview(ovalImageView3)
        
        ovalImageView4       = UIImageView(image:ovalImage)
        ovalImageView4.layer.position = CGPoint(x:0, y:0)
        ovalImageView4.frame.size = CGSize(width: 0, height: 0)
        self.view.addSubview(ovalImageView4)
        
        ovalImageView5       = UIImageView(image:ovalImage)
        ovalImageView5.layer.position = CGPoint(x:0, y:0)
        ovalImageView5.frame.size = CGSize(width: 0, height: 0)
        self.view.addSubview(ovalImageView5)
        
        self.reloadImage = UIImage(named: "oval")!
        reloadImageView       = UIImageView(image:self.reloadImage)
        reloadImageView.layer.position = CGPoint(x:0, y:0)
        reloadImageView.frame.size = CGSize(width: 0, height: 0)
        self.view.addSubview(reloadImageView)
        
        // いいねアイコン5種類を設定
        self.reloadImage = UIImage(named: "rewind")!
        reloadImageView       = UIImageView(image:self.reloadImage)
        reloadImageView.layer.position = CGPoint(x:0, y:0)
        reloadImageView.frame.size = CGSize(width: 0, height: 0)
        self.view.addSubview(reloadImageView)
        
        self.barbelImage = UIImage(named: "barbel")!
        barbelImageView       = UIImageView(image:self.barbelImage)
        barbelImageView.layer.position = CGPoint(x:0, y:0)
        barbelImageView.frame.size = CGSize(width: 0, height: 0)
        self.view.addSubview(barbelImageView)
        
        self.armImage = UIImage(named: "arm")!
        armImageView       = UIImageView(image:self.armImage)
        armImageView.layer.position = CGPoint(x:0, y:0)
        armImageView.frame.size = CGSize(width: 0, height: 0)
        self.view.addSubview(armImageView)
        
        self.proteinImage = UIImage(named: "protein")!
        proteinImageView       = UIImageView(image:self.proteinImage)
        proteinImageView.layer.position = CGPoint(x:0, y:0)
        proteinImageView.frame.size = CGSize(width: 0, height: 0)
        self.view.addSubview(proteinImageView)
        
        self.bodyImage = UIImage(named: "body")!
        bodyImageView       = UIImageView(image:bodyImage)
        bodyImageView.layer.position = CGPoint(x:0, y:0)
        bodyImageView.frame.size = CGSize(width: 0, height: 0)
        self.view.addSubview(bodyImageView)
        
        // 制約を設定
        constrain(self.kolodaView,ovalImageView1) { view1,view2 in
            view2.top == view1.bottom + 10
            view2.left == view1.left
        }
        
        // 制約を設定
        constrain(self.kolodaView,ovalImageView5) { view1,view2 in
            view2.top == view1.bottom + 10
            view2.right == view1.right
        }
        
        // 制約を設定
        constrain(self.kolodaView,ovalImageView3) { view1,view2 in
            view2.top == view1.bottom + 10
            view2.centerX == view1.centerX
        }
        
        // 制約を設定
        constrain(ovalImageView1,ovalImageView2,ovalImageView3,ovalImageView4,ovalImageView5) { view1,view2,view3,view4,view5 in
            view2.top == view1.top
            view4.top == view1.top
            
            // width 全てovalImageView1と同じ大きさにする
            view2.width == view1.width
            view3.width == view1.width
            view4.width == view1.width
            view5.width == view1.width
            
            // Distributes
            distribute(by:CGFloat(self.viewW*0.093), horizontally: view1,view2 ,view3,view4 ,view5)
        }
        
        // 制約を設定
        constrain(ovalImageView1,self.reloadImageView) { view1,view2 in
            view2.centerY == view1.centerY
            view2.centerX == view1.centerX
        }
        
        // 制約を設定
        constrain(ovalImageView2,self.barbelImageView) { view1,view2 in
            view2.centerY == view1.centerY
            view2.centerX == view1.centerX
        }
        
        // 制約を設定
        constrain(ovalImageView3,self.armImageView) { view1,view2 in
            view2.centerY == view1.centerY
            view2.centerX == view1.centerX
        }
        
        // 制約を設定
        constrain(ovalImageView4,self.proteinImageView) { view1,view2 in
            view2.centerY == view1.centerY
            view2.centerX == view1.centerX
        }
        
        // 制約を設定
        constrain(ovalImageView5,self.bodyImageView) { view1,view2 in
            view2.centerY == view1.centerY
            view2.centerX == view1.centerX
        }
    }
    
    func randomColor() -> UIColor {
        return UIColor(colorLiteralRed: randomFloat(), green: randomFloat(), blue: randomFloat(), alpha: 1)
    }
    func randomFloat() -> Float {
        return Float(Float(arc4random()) / Float(UInt32.max))
    }

        // 各種ビューの設定
    func laodCardData(){

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension UIViewController: KolodaViewDataSource {
    public func kolodaNumberOfCards(_ koloda: KolodaView) -> Int {
        return 10
    }
    public func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
        let view = UIView(frame: koloda.bounds)
        view.backgroundColor = UIColor.gray
        return view
    }
}

