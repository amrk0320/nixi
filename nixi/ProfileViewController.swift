//
//  ProfileViewController.swift
//  nixi
//
//  Created by munetomoissei on 2017/08/12.
//  Copyright © 2017年 munetomoissei. All rights reserved.
//

import UIKit
import Cartography

class ProfileViewController: UIViewController, UITextFieldDelegate,UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    let headerView = UIView()
    let profileContentView = UIView()
    var viewW = 0.0

    override func viewDidLoad() {
        
        super.viewDidLoad()

        // コンテンツを設定する
        self.setViewContent()
        
        self.setProfileConten()
    }
    
    func setProfileConten()  {
        
        profileContentView.frame = CGRect(x:0, y:0, width:0, height:0)
        profileContentView.layer.borderWidth = 1.0
        profileContentView.layer.borderColor = UIColor.clear.cgColor
        self.view.addSubview(profileContentView)
        profileContentView.alpha = 1.0


        // 制約を設定
        constrain(profileContentView) { view1 in
            view1.width == (view1.superview?.width)!*0.9
            view1.height == (view1.superview?.height)!*0.125
            view1.centerX == (view1.superview?.centerX)!
        }
        
        // 制約を設定
        constrain(profileContentView,headerView) { view1,view2 in
             view1.top == view2.bottom + 20
        }
        
        // 投稿画像
        let profileImage:UIImage? = UIImage(named: "ronald.jpg")
        let profileImageView       = UIImageView(image:profileImage)
        profileImageView.frame = CGRect(x:0, y:0, width:0, height:0)
        profileImageView.clipsToBounds = true
        profileImageView.layer.cornerRadius = 35
        profileContentView.addSubview(profileImageView)
        
        // 制約を設定
        constrain(profileImageView) { view1 in
            view1.width == (view1.superview?.width)!*0.25
            view1.height == (view1.superview?.width)!*0.25
        }
        
        // 制約を設定
        constrain(profileContentView,profileImageView) { view1,view2 in
            view2.left == view1.left
            view2.top == view1.top
        }
        
        // nixiPointラベルを設定
        let nixiPointLabel = UILabel()
        nixiPointLabel.frame = CGRect(x:200, y:0, width:0, height:0)
        nixiPointLabel.text = "200"
        nixiPointLabel.font = UIFont.systemFont(ofSize: 20.0)
        nixiPointLabel.sizeToFit()
        profileContentView.addSubview(nixiPointLabel)
        
        // いいね数のラベルを設定
        let goodPontLabel = UILabel()
        goodPontLabel.frame = CGRect(x:0, y:0, width:0, height:0)
        goodPontLabel.text = "1032"
        goodPontLabel.font = UIFont.systemFont(ofSize: 20.0)
        profileContentView.addSubview(goodPontLabel)
        
        // 全評価のラベルを設定
        let zenhyokaLabel = UILabel()
        zenhyokaLabel.frame = CGRect(x:0, y:0, width:0, height:0)
        zenhyokaLabel.text = "2000"
        zenhyokaLabel.font = UIFont.systemFont(ofSize: 20.0)
        profileContentView.addSubview(zenhyokaLabel)
        
        // nixiPointラベルを設定
        let nixiPointNameLabel = UILabel()
        nixiPointNameLabel.frame = CGRect(x:0, y:0, width:0, height:0)
        nixiPointNameLabel.text = "直近のnixi値"
        nixiPointNameLabel.font = UIFont.systemFont(ofSize: 15.0)
        nixiPointNameLabel.sizeToFit()
        nixiPointNameLabel.textColor = UIColor(red:171.0/255,green:171.0/255,blue:171.0/255,alpha:1.0)
        profileContentView.addSubview(nixiPointNameLabel)
        
        // いいね数のラベル(名前)を設定
        let goodPontNameLabel = UILabel()
        goodPontNameLabel.frame = CGRect(x:0, y:0, width:0, height:0)
        goodPontNameLabel.text = "いいね数"
        goodPontNameLabel.font = UIFont.systemFont(ofSize: 15.0)
        goodPontNameLabel.textColor = UIColor(red:171.0/255,green:171.0/255,blue:171.0/255,alpha:1.0)
        profileContentView.addSubview(goodPontNameLabel)
        
        // 全評価のラベルを設定
        let zenhyokaNameLabel = UILabel()
        zenhyokaNameLabel.frame = CGRect(x:0, y:0, width:0, height:0)
        zenhyokaNameLabel.text = "全評価"
        zenhyokaNameLabel.font = UIFont.systemFont(ofSize: 15.0)
        zenhyokaNameLabel.textColor = UIColor(red:171.0/255,green:171.0/255,blue:171.0/255,alpha:1.0)
        profileContentView.addSubview(zenhyokaNameLabel)
        
        // 制約を設定
        constrain(profileContentView,nixiPointLabel,goodPontLabel,zenhyokaLabel,profileImageView) { view1,view2,view3,view4,view5 in
            view2.top == view1.top
            view3.top == view1.top
            view4.top == view1.top
            view4.right == view1.right
            distribute(by:CGFloat(self.view.frame.width*0.093), horizontally: view2 ,view3,view4)
        }

        // 制約を設定
        constrain(nixiPointLabel,nixiPointNameLabel) { view1,view2 in
            view2.centerX == view1.centerX
            view2.top == view1.bottom + 5
        }
        
        // 制約を設定
        constrain(goodPontLabel,goodPontNameLabel) { view1,view2 in
            view2.centerX == view1.centerX
            view2.top == view1.bottom + 5
        }

        // 制約を設定
        constrain(zenhyokaLabel,zenhyokaNameLabel) { view1,view2 in
            view2.centerX == view1.centerX
            view2.top == view1.bottom + 5
        }

        // プロフィール編集ボタンの設置
        var editProfileButton = UIButton()
        editProfileButton = UIButton(frame: CGRect(x: 0, y: 0,width: 0,height: 0))
        editProfileButton.setTitle("EDIT YOUR PROFILE", for: UIControlState())
        editProfileButton.setTitleColor(UIColor.black, for: UIControlState())
        editProfileButton.titleLabel!.font = UIFont(name: "STHeitiTC-Light",size: CGFloat(14))
        editProfileButton.layer.borderColor = UIColor.clear.cgColor
        editProfileButton.titleLabel!.font = UIFont(name: "STHeitiTC-Light",size: CGFloat(14))
        editProfileButton.layer.masksToBounds = true
        editProfileButton.layer.cornerRadius = 4.0
        // editProfileButton.layer.borderColor = placeColor.cgColor
        editProfileButton.backgroundColor = UIColor(red:121.0/255,green:121.0/255,blue:121.0/255,alpha:1.0)
        // editProfileButton.addTarget(self, action: #selector(self.send(_:)), for: .touchUpInside)
        profileContentView.addSubview(editProfileButton)

        // 制約を設定
        constrain(editProfileButton,profileContentView,goodPontLabel) { view1,view2,view3 in
            view1.bottom == view2.bottom  
            view3.centerX == view1.centerX
            view1.width == (view1.superview?.width)!*0.65
            view1.width == (view1.superview?.height)!*0.041
        }

        // ひとことラベルを設定
        let hitokotoLabel = UILabel()
        hitokotoLabel.frame = CGRect(x:0, y:0, width:0, height:0)
        hitokotoLabel.text = "200"
        hitokotoLabel.font = UIFont.systemFont(ofSize: 15.0)
        hitokotoLabel.sizeToFit()
        self.view.addSubview(hitokotoLabel)

        // 制約を設定
        constrain(hitokotoLabel,profileContentView) { view1,view2 in
            view1.left == view2.left  
            view1.top == view2.bottom + 30
            view1.width == (view1.superview?.width)!
        }
    }
    
    // 各種ビューの設定
    func setViewContent(){
        
        // 背景色
        self.view.backgroundColor = UIColor.white
        
        // ヘッダーの設定
        headerView.frame = CGRect(x:0, y:0, width:0, height:0)
        headerView.backgroundColor = UIColor(red:208.0/255,green:1.0/255,blue:27.0/255,alpha:1.0)
        self.view.addSubview(headerView)
        // 制約を設定
        constrain(headerView) { view in
            view.height == view.superview!.height*0.08
            view.width == view.superview!.width
        }
        
        // ロゴ画像
        let nameLabel:UILabel? = UILabel()
        nameLabel?.frame = CGRect(x:0, y:0, width:0, height:0)
        nameLabel?.text = "@fukamachi"
        nameLabel?.font = UIFont.systemFont(ofSize: 15.0)
        nameLabel?.textColor = UIColor.white
        headerView.addSubview(nameLabel!)
        
        // 制約を設定
        constrain(nameLabel!) { view in
            view.center == view.superview!.center
        }
        
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width:50, height:50)//大きさ
        layout.sectionInset = UIEdgeInsetsMake(16, 16, 32, 16)//マージン
        layout.headerReferenceSize = CGSize(width:100,height:30)
        let myCollectionView = UICollectionView(frame: CGRect(x:0, y:self.view.frame.height*0.391, width:self.view.frame.width, height:300), collectionViewLayout: layout)
        myCollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
        myCollectionView.delegate = self
        myCollectionView.dataSource = self
        myCollectionView.backgroundColor = UIColor.clear
        self.view.addSubview(myCollectionView)

        // 画像グリッドビューに対して3分割のレイアウトを設定
        let inset = UIEdgeInsetsMake(10.0, 0.0, 10.0, 0.0)
        myCollectionView.adaptBeautifulGrid(numberOfGridsPerRow: 3, gridLineSpace: 1.0, sectionInset: inset)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // 改行が入力された
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("push Return")
        // キーボードを下げる
        view.endEditing(true)
        return false
    }
    
    // テキストフィールド編集
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("change text field")
        return true
    }

        //Cellがクリックされた時によばれます
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("選択しました: \(indexPath.row)")
    }
    
    //Cellの合計数
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    //collectionViewのセルを返す（必須）
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath)

    //背景色指定
    // 投稿画像
    let image:UIImage? = UIImage(named: "dns.jpeg")
    let postImage       = UIImageView(image:image)
    postImage.frame = CGRect(x:0, y:0, width:0, height:0)
    postImage.clipsToBounds = true
    cell.backgroundView = postImage

    //タグを手掛かりにラベルを特定する
//    let textLabel = cell.contentView.viewWithTag(1) as! UILabel
//    textLabel.text = "test"
    return cell
    }

}

public extension UICollectionView {

    public func adaptBeautifulGrid(numberOfGridsPerRow: Int, gridLineSpace space: CGFloat) {
        let inset = UIEdgeInsets(
            top: space, left: space, bottom: space, right: space
        )
        adaptBeautifulGrid(numberOfGridsPerRow: numberOfGridsPerRow, gridLineSpace: space, sectionInset: inset)
    }

    public func adaptBeautifulGrid(numberOfGridsPerRow: Int, gridLineSpace space: CGFloat, sectionInset inset: UIEdgeInsets) {
        guard let layout = collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }
        guard numberOfGridsPerRow > 0 else {
            return
        }
        let isScrollDirectionVertical = layout.scrollDirection == .vertical
        var length = isScrollDirectionVertical ? self.frame.width : self.frame.height
        length -= space * CGFloat(numberOfGridsPerRow - 1)
        length -= isScrollDirectionVertical ? (inset.left + inset.right) : (inset.top + inset.bottom)
        let side = length / CGFloat(numberOfGridsPerRow)
        guard side > 0.0 else {
            return
        }
        layout.itemSize = CGSize(width: side, height: side)
        layout.minimumLineSpacing = space
        layout.minimumInteritemSpacing = space
        layout.sectionInset = inset
        layout.invalidateLayout()
    }

}
